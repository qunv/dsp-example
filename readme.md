# Warning

+ Overuse of design patterns can lead to code that is downright over-engineered.
- Always go with the simplest solution that does the job and introduce patterns only where the need emerges.

# Introduction

+ Designing object-oriented software is hard, designing reusable object-oriented software is even harder
- Design should be specific to problem, but also general enough to address future problems and requirements
* Expert designers reuse solutions that have worked for them in the past
  + Recurring patterns of classes and communicating objects exist in many object-oriented systems
+ If details of previous problems and their solutions are known, then they could be reused
  * Recording experience in software design for others to use
- Design patterns = important and recurring design in object-oriented systems

# Design Pattern

> "Each pattern describes a problem which occurs
over and over again in our environment and
then describes the core of the solution to that
problem, in such a way that you can use this
solution a million times over, without ever doing
it in the same way twice" - Christopher Alexander, A Pattern Language,
1977

A Pattern has 4 Essential Elements
- Pattern name
+ Problem
* Solution
- Consequences

# Design Patterns Are Not

+ Designs that can be encoded in classes and reused as is (i.e. linked lists, hash tables)
- Complex domain-specific designs (for an entire application or subsystem)
* They Are: “Descriptions of communicating objects and classes that are customized to solve a general design problem in a particular context.”

# Good Uses For Design Patterns

+ Let design patterns emerge from your design,
don’t use them just because you should
- Always choose the simplest solution that meets
your need
* Always use the pattern if it simplifies the solution
+ Know all the design patterns out there
- Talk a pattern when that simplifies conversation

## Patterns:

### Behavioral Pattern

- [ChainOfResponsibility](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Behavioral/ChainOfResponsibility)
- [Command](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Behavioral/Command)
- [Iterator](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Behavioral/Iterator)
- [Observer](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Behavioral/Observer)
- [State](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Behavioral/State)
- [Strategy](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Behavioral/Strategy)

### Creational Pattern

- [Builder](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Creational/Builder)
- [Factory](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Creational/Factory)
- [Singleton](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Creational/Singleton)

### Structural Pattern

- [Adapter](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Structural/Adapter)
- [Bridge](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Structural/Bridge)
- [Composite](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Structural/Composite)
- [Decorator](https://gitlab.com/qunv/dsp-example/-/tree/main/src/Structural/Decorator)
